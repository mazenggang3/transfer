# Syzkaller OpenEuler Test

Basic on google/syzkaller and qemu-kvm tools, is designed for openEuler kernel test.  
**support x86_64 and aarch64 architectures only**

## Steps
1. Download and put openEuler ISO file under layout ./iso
2. run ./main iso/xxx.yy.zz.iso

## how does the main script work?
In a word, use the ISO file to create a basic qemu-kvm qcow2 image and
call tool syzkaller to boot several qemu test kvms.

### result
syzkaller work layout: `$PWD/$(date +%Y%m%d%H%M%S)`

## Help
### attentions
1. when build kernel, you need enter choices manually
2. when qemu-kvm boot in os installing, setup and press `Enter` to reboot
3. when x86_64 machines first boot from qcow2 image, you need modify grub configuration and then shutdown.

### x86_64
1. when qemu-kvm boot from ISO
![pxelinux-menu-x64](https://gitee.com/angus_robot/imageBed/raw/master/img/pxelinux-menu.png)
press `TAB`  
![](https://gitee.com/angus_robot/imageBed/raw/master/img/raw-set-x64.png)
modify grub option with command `e` as below:
![](https://gitee.com/angus_robot/imageBed/raw/master/img/new-set-x64.png)
then `ctrl x` for boot with current configuration
![](https://gitee.com/angus_robot/imageBed/raw/master/img/install-set-x64.png)

2. when qemu-kvm boot from qcow2 image  
modify grub configuration as below:
![](https://gitee.com/angus_robot/imageBed/raw/master/img/modify-grub.png)

### aarch64
1. when qemu-kvm boot from ISO
![grub-menu-aarch64](https://gitee.com/angus_robot/imageBed/raw/master/img/grub-menu.png)
![raw-set-aarch64](https://gitee.com/angus_robot/imageBed/raw/master/img/raw-set.png)
modify grub option with command `e` as below:
![new-set-aarch64](https://gitee.com/angus_robot/imageBed/raw/master/img/new-set.png)
then `ctrl x` for boot with current configuration
![install-set-aarch64](https://gitee.com/angus_robot/imageBed/raw/master/img/install-set.png)
