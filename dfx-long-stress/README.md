# DFX Stress Test
testsuits named `arch-4/5.x.tar.gz` compressed with tar zcf.

```text
-- dfx-long-stree
---- aarch64-4.x.tar.gz    # old kernel version test
---- aarch64-5.x.tar.gz    # new kernel version test
---- x86_64-4.x.tar.gz
---- x86_64-5.x.tar.gz
---- common
---- main                  # script local running
---- dfx-test              # called in main and remote running
---- prepare-kernel        # called in dfx-test when test new kernel
```
