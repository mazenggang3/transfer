# deploy basic environment
shell(bash, zsh, tmux) and ssh environment configuration.  

```bash
./send-deploy 172.168.131.15
```
script will auto ssh and copy config files from local to remote host specified, config dnf or
yum repo and install some basic packages.
